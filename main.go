package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/ikawaha/kagome/tokenizer"
	"github.com/sclevine/agouti"
)

const (
	ERR_err = "エラー"

	SEARCH_URL = "https://www.google.co.jp/search?source=hp&q=%E5%B1%B1%E5%BD%A2%E7%9C%8C+%E5%8D%97%E9%99%BD%E5%B8%82&oq=%E5%B1%B1%E5%BD%A2%E7%9C%8C+%E5%8D%97%E9%99%BD%E5%B8%82"
	NEXT_URL   = "&start="
)

func main() {
	// # 概要
	// - agoutiでアクセスしてURL取得
	// - goqueryにURLを渡して、必要情報取得
	// - PhantomJSブラウザではJavaScript無効
	// - 取得した文字列を./db/text.txtに保存
	// - 上記ファイルを読み出して、Shift-JISをUTF-8
	// - 形態素解析にかける

	// 検索したものを.txtに保存する
	title, text := GetTextAtGoogle()

	ChangeFileForKeywords("title", title)
	ChangeFileForKeywords("text", text)
}

// Google search フォームに入力
// if err := page.FindByName("q").Fill("山形県 南陽市"); err != nil {
//   log.Error(err, ERR_err)
// }
// if err := page.FindByName("gs").Submit(); err != nil {
//   log.Error(err, ERR_err)
// }

// Google search 必要情報
// s := page.Find("span").FirstByClass("st").String()
// page.Screenshot("./screenshot/st.png")
// p(s)

func GetTextAtGoogle() ([]string, []string) {
	// Agouti.New
	agoutiDriver := agouti.PhantomJS()
	agoutiDriver.Start()
	defer agoutiDriver.Stop()
	page, err := agoutiDriver.NewPage()
	if err != nil {
		fmt.Println(err)
	}

	var title []string
	var text []string
	for i := 0; i < 30; i++ {
		if i == 0 {
			page.Navigate(SEARCH_URL)
			page.Screenshot(fmt.Sprintf("./screenshot/url_%d.png", i))

			s, _ := page.Find("body").Attribute("innerHTML")
			stringReader := strings.NewReader(s)
			doc, _ := goquery.NewDocumentFromReader(stringReader)

			doc.Find("h3.r").Each(func(i int, s *goquery.Selection) {
				title = append(title, s.Text())
			})
			doc.Find("span.st").Each(func(_ int, s *goquery.Selection) {
				text = append(text, s.Text())
			})
		} else {
			page.Navigate(fmt.Sprintf("%s%s%d", SEARCH_URL, NEXT_URL, i*10))
			page.Screenshot(fmt.Sprintf("./screenshot/url_%d.png", i))

			s, _ := page.Find("body").Attribute("innerHTML")
			stringReader := strings.NewReader(s)
			doc, _ := goquery.NewDocumentFromReader(stringReader)

			doc.Find("h3.r").Each(func(_ int, s *goquery.Selection) {
				title = append(title, s.Text())
			})
			doc.Find("span.st").Each(func(_ int, s *goquery.Selection) {
				text = append(text, s.Text())
			})
		}
	}

	j1, _ := json.Marshal(title)
	j2, _ := json.Marshal(text)

	ioutil.WriteFile("./db/title.txt", j1, 0755)
	ioutil.WriteFile("./db/text.txt", j2, 0755)

	return title, text
}

func ChangeFileForKeywords(s string, this []string) {
	// //read from stream (Shift-JIS to UTF-8)
	// scanner := bufio.NewScanner()
	// var list string
	// for scanner.Scan() {
	// 	list += scanner.Text()
	// }
	// if err := scanner.Err(); err != nil {
	// 	fmt.Println(err)
	// }

	// イニシャライズ
	t := tokenizer.New()
	// mecab 形態素解析
	var texts string

	for _, v := range this {
		tokens := t.Analyze(v, tokenizer.Normal)
		for _, token := range tokens {
			if token.Class == tokenizer.DUMMY {
				// BOS: Begin Of Sentence, EOS: End Of Sentence.
				fmt.Printf("%s\n", token.Surface)
				continue
			}
			a := strings.Join(token.Features(), ",")
			b := fmt.Sprintf("%s\n", a)
			texts += b
		}
	}

	ioutil.WriteFile(fmt.Sprintf("./db/word_%s.txt", s), []byte(texts), 0755)
}
